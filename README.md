# Test

Programming test for Data Science and Control Theory

Work on a small project on analysis and model building on a gas plant compressor data. A sheet containing data for a compressor building in a gas pumping station in Canada is present in K300 Compressor Data.xlsx. The data is for 1 day(Feb 1st). The Tag description and Compressor Building P&ID diagram are added to make it easy to understand. 

Work to be done. 
1. Analysis of stability of the Compressor
2. Model building for discharge pressures after Stage 1, Stage 2 and Stage 3

Your approach to handling this project will scored. 
Please create functions which can be further used to run on the remaining 27 days of Feb.

Please let us know of there are any questions. Call us if any background information on the data is needed.

contact: careers@ntwist.com